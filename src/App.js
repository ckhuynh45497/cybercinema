import React from 'react';

import './App.css';
import HeaderMovie from './components/Navbar';


function App() {
  return (
    <div className="App">
      <HeaderMovie/>
    </div>
  );
}

export default App;
