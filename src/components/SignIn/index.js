import React, { useEffect } from "react";
import Input from "@material-ui/core/Input";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Button from "@material-ui/core/Button";

import Grid from "@material-ui/core/Grid";

import FormHelperText from "@material-ui/core/FormHelperText";
import { withFormik, Form, Field } from "formik";
import * as Yup from "yup";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      width: "90%",
      margin: "0 auto",
    },
    "& label": {
      color: "orange",
      fontSize: "14px",
    },
    "& label.Mui-focused": {
      color: "orange",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "orange",
    },
    "& .MuiInput-underline:before": {
      borderBottomColor: "orange",
    },
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
      borderBottomColor: "#b2ff4f",
    },
    flexGrow: 1,
  },
  textField: {
    margin: "10%",
  },
  button: {
    backgroundColor: "orange",
    margin: "10%",
    color: "White",
    width: "80%",
    "&:hover": {
      backgroundColor: "orange",
    },
    "&:focus": {
      outline: "none",
    },
  },
}));

function SignIn(props) {
  const classes = useStyles();

  function handleSubmit() {
    if (props.dirty && props.isValid) {
      axios({
        method: "POST",
        url: "http://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
        data: props.values,
      })
        .then((res) => {
          console.log(res.data);
          localStorage.setItem("credential", JSON.stringify(res));
        })
        .catch((err) => {
          console.log(err);
        });
    }
    // console.log(props.values)
  }
  useEffect(() => {
    //useEffect = life cycle didupdate
    console.log("useEffect Didupdate");
  }, [props.values]);
  return (
    <Form className={classes.root}>
      <Grid container justify="center" alignContent="center">
        <FormControl
          fullWidth
          size="medium"
          margin="normal"
          error={props.touched.taiKhoan && !!props.errors.taiKhoan}
        >
          <InputLabel>Tài Khoản</InputLabel>
          <Field
            name="taiKhoan"
            render={({ field }) => <Input className="pl-2" fullWidth {...field} />}
          />
          {props.touched.taiKhoan && (
            <FormHelperText>{props.errors.taiKhoan}</FormHelperText>
          )}
        </FormControl>
        <FormControl
          fullWidth
          margin="normal"
          error={props.touched.matKhau && !!props.errors.matKhau}
        >
          <InputLabel>Mật Khẩu</InputLabel>
          <Field
            name="matKhau"
            render={({ field }) => (
              <Input className="pl-2" fullWidth type="password" {...field} />
            )}
          />
          {props.touched.matKhau && (
            <FormHelperText>{props.errors.matKhau}</FormHelperText>
          )}
        </FormControl>
        <FormControl fullWidth margin="normal">
          <Button
            className={classes.button}
            variant="extendedFab"
            color="primary"
            type="submit"
            onClick={handleSubmit}
          >
            Signup
          </Button>
        </FormControl>
      </Grid>
    </Form>
  );
}

const FormikForm = withFormik({
  mapPropsToValues() {
    return {
      taiKhoan: "",
      matKhau: "",
    };
  },
  validationSchema: Yup.object().shape({
    taiKhoan: Yup.string()
      .required("Vui lòng nhập Tài Khoản")
      .min(6, " Vui lòng nhập hơn 6 ký tự")
      .max(15, " Vui lòng nhập không quá 15 ký tự"),
    matKhau: Yup.string()
      .required("Vui lòng nhập Mật Khẩu")
      .min(6, " Vui lòng nhập hơn 6 ký tự")
      .max(15, " Vui lòng nhập không quá 15 ký tự"),
  }),
})(SignIn);

export default FormikForm;
