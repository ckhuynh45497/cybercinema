import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import LogInTabs from "../Tabs";
import './style.css'

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "30%",
    marginLeft: "35%",
  },
  
}));

function HeaderMovie() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  return (
    <>
      <Navbar bg="dark" variant="dark" expand="lg" className="position-fixed" style={{width:"100%"}}>
        <div className="container">
          <Navbar.Brand style={{color:"orange"}}>CyberCinema</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className={`${"ml-auto"}`}>
              <Nav.Link>TRANG CHỦ</Nav.Link>
              <Nav.Link>LỊCH CHIẾU</Nav.Link>
              <Nav.Link>CỤM RẠP</Nav.Link>
              <Nav.Link>TINH TỨC</Nav.Link>
              <Nav.Link>LIÊN HỆ</Nav.Link>
              <Nav.Link onClick={handleOpen}>ĐĂNG NHẬP</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </div>
      </Navbar>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <LogInTabs/>
        </Fade>
      </Modal>
    </>
  );
}

export default HeaderMovie;
